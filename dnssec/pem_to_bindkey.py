#!/usr/bin/env python3
"""
TODO : have a look to dnssec-keygen for details on timing stuff
in private key file

Requires dnspython ≥ 2.3.0

Takes a a PEM-encoded private key file and turns it in
private key format used by BIND or other DNS tools such a ldns
generated using dnssec-keygen from Bind. The key file created is only
readable by its owner and is not writable

Also generates .key and .ds files derived from the private key and
containing :
    - The DNSKEY record
    - Optionally, the DS record for the domain given in input

ldns-keygen generates that DS record if user choose to create a KSK,
dnssec-keygen does not, so the option is up to the user.

Follows RFC 8624 section 3.1 recommendations by excluding
algorithms listed as 'MUST NOT' and 'NOT RECOMMENDED' for DNSSEC signing,
thus allowed algorithms are 8 and 13 to 16.

Therefore, the program choose DNSSEC algorithm automatically according to
the type of key given as input

"""

from typing import Optional

import sys
import argparse
import base64
import textwrap

from math import ceil
from getpass import getpass
from pathlib import Path

from datetime import datetime, timedelta

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa, ec, ed25519, ed448

from dns import dnssec

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                description=textwrap.dedent("""\
        Takes a PEM-encoded private key files and converts it to a BIND9 private key file format
        2 files are created by default :
            - A .private file containing the private key
            - A .key file containing the public key in RR format

        Optionally a .ds file containing the DS record in RR format can be created

        DNSSEC algorithm is chosen automatically. For RSA key, only RSASHA256 (algorithm 8) is allowed. """),
                                 epilog="Basename for output files is K<domain>+<alg>+<id>")
parser.add_argument("-i", "--input", help="Key file to transform", required=True, metavar="FILE", type=Path)
parser.add_argument("-o", "--output", help="Path where to write output files. Default: same as input", metavar="PATH", type=Path)
parser.add_argument("-k", help="If set, the key will be flagged as a KSK (flag 257) in the .key file", action="store_true")
parser.add_argument("-d", "--ds", help="If set, a .ds file with the DS record of the DNSKEY record will be created.", action="store_true")
parser.add_argument("-p", "--password", help="If key file is encrypted, user will be prompted for PEM pass phrase",
                    action="store_true")
parser.add_argument("-s", "--secret", help="If key file is encrypted, reads password from a file. Keep it secret, keep it safe",
                    metavar="FILE", type=Path)
parser.add_argument("-f", "--force", help="Force overwriting private file if it already exists. .key and .ds are always overwritten", action="store_true")
parser.add_argument("domain", help="If domain name is an IDN, please use punycode to avoid trouble", type=str)
args = parser.parse_args()

# We make sure the Path is absolute
keyFile = args.input.resolve()

if args.output:
    outPath = args.output.resolve()
else:
    outPath = keyFile.parent

# Setting DNSSEC stuff
if args.k:
    flag = 257
    keyType = "ksk"
else:
    flag = 256
    keyType = "zsk"

dsDigest = "SHA256"

# Password stuff

if args.password or args.secret:
    encrypted = True
else:
    encrypted = False

if args.secret:
    passFile = args.secret.resolve()
else:
    passFile = None

# DNS names are not case sensitive but in key file name, lowercase is used
# Both dnssec-keygen and ldns-keygen don't seem to handle IDNs. We do
# not want to go there hence the help message in 'domain' description
if args.domain.endswith("."):
    domain = args.domain.lower()
else:
    domain = args.domain.lower() + "."

def input_password(file: Optional[Path] = None) -> bytes:
    if file is not None:
        return file.read_bytes().strip(b'\n')
    else:
        p1 = getpass("Enter PEM pass phrase:").encode()
        return p1

def int_to_b64(i: int) -> str:
    """Takes an int and returns a nice PEM encoded base64 string"""
    length = ceil(i.bit_length() / 8)
    blob = base64.b64encode(i.to_bytes(length, byteorder="big")).decode()
    return blob

if __name__ == "__main__":
    if not keyFile.exists():
        raise FileNotFoundError(f"No such file '{keyFile}'")

    output="Private-key-format: v1.3\n"

    # Load pem file
    print(f"Loading '{keyFile}'")
    pem = keyFile.read_bytes()
    if encrypted:
        while True:
            try:
                privateKey = serialization.load_pem_private_key(pem, input_password(passFile))
            except ValueError:
                print("Cannot decode key. Bad Password? Please try again")
                continue
            break
    else:
        privateKey = serialization.load_pem_private_key(pem, None)

    publicKey = privateKey.public_key()

    # What algo ?
    if isinstance(privateKey, rsa.RSAPrivateKey):
        algo = (8, "RSASHA256")
        size = privateKey.key_size
        print(f"{size}-bit RSA key")
    elif isinstance(privateKey, ec.EllipticCurvePrivateKey):
        if isinstance(privateKey.curve, ec.SECP256R1):
            algo = (13, "ECDSAP256SHA256")
            size = 256
            print("ECDSA with curve P-256 key")
        elif isinstance(privateKey.curve, ec.SECP384R1):
            algo = (14, "ECDSAP384SHA384")
            size = 384
            dsDigest = "SHA384"
            print("ECDSA with curve P-384 key")
        else:
            raise ValueError(f"curve {type(privateKey.curve).__name__} is unsupported")
    elif isinstance(privateKey, ed25519.Ed25519PrivateKey):
        algo = (15, "ED25519")
        size = 256
        print("EdDSA with curve ed25519 key")
    elif isinstance(privateKey, ed448.Ed448PrivateKey):
        algo = (16, "ED448")
        size = 456
        print("EdDSA with curve ed448 key")
    else:
        raise TypeError(f"Algorithm {type(privateKey).__name__} is unsupported")
    output+=f"Algorithm: {algo[0]} ({algo[1]})\n"

    # Now the fun part : extracting the values we need
    # RSA
    if algo[0] == 8:
        if privateKey.private_numbers().public_numbers.e != 65537:
            raise ValueError("Public exponent is not equal to 65537!")
        prime1 = int_to_b64(privateKey.private_numbers().p)
        prime2 = int_to_b64(privateKey.private_numbers().q)
        privateExponent = int_to_b64(privateKey.private_numbers().d)
        exponent1 = int_to_b64(privateKey.private_numbers().dmp1)
        exponent2 = int_to_b64(privateKey.private_numbers().dmq1)
        coefficient = int_to_b64(privateKey.private_numbers().iqmp)
        modulus = int_to_b64(privateKey.private_numbers().public_numbers.n)
        # Dumping to output
        output+=f"Modulus: {modulus}\n"
        output+="PublicExponent: AQAB\n"
        output+=f"PrivateExponent: {privateExponent}\n"
        output+=f"Prime1: {prime1}\n"
        output+=f"Prime2: {prime2}\n"
        output+=f"Exponent1: {exponent1}\n"
        output+=f"Exponent2: {exponent2}\n"
        output+=f"Coefficient: {coefficient}\n"
    # ECDSA
    elif algo[0] in (13,14):
        prime = int_to_b64(privateKey.private_numbers().private_value)
        output+=f"PrivateKey: {prime}\n"
    # Ed25519 / Ed448
    else:
        # Values are byte-encoded with EdDSA keys
        prime = base64.b64encode(privateKey.private_bytes(
            serialization.Encoding.Raw,
            serialization.PrivateFormat.Raw,
            serialization.NoEncryption())).decode()
        output+=f"PrivateKey: {prime}\n"

    # Completing output with Created / Publish & Activate dates
    # dnssec-keygen uses 'now' + 10 minutes. We'll stick to that
    date = (datetime.now() + timedelta(minutes = 10)).strftime("%Y%m%d%H%M%S")
    output+=f"Created: {date}\n"
    output+=f"Publish: {date}\n"
    output+=f"Activate: {date}\n"

    # Creating the DNSKEY record
    # dnspython .to_text() adds spaces in the key part of the RDATA which is infortunate
    # So generating DNSKEY is only useful for keytag
    dnskey = dnssec.make_dnskey(publicKey, algo[0], flag)
    keytag = dnssec.key_id(dnskey)
    ds = dnssec.make_ds(domain, dnskey, dsDigest, validating=True)

    # Writing output
    basename = f"K{domain}+{algo[0]:03}+{keytag:05}"

    outPrivate = outPath.joinpath(f"{basename}.private")
    outPubKey = outPath.joinpath(f"{basename}.key")
    outDS = outPath.joinpath(f"{basename}.ds")

    if outPrivate.exists():
        if args.force:
            outPrivate.chmod(0o600)
        else:
            confirm = input(f"WARNING: '{outPrivate}' already exists\nOverwrite it? (y/N)")
            if confirm in ("y", "Y"):
                outPrivate.chmod(0o600)
            else:
                print("Not overwriting file. Bye")
                sys.exit(2)

    print(f"Writing private key to '{outPrivate}'")
    outPrivate.write_text(output)
    outPrivate.chmod(0o400)
    print(f"Writing DNSKEY RR to '{outPubKey}'")
    outPubKey.write_text(f"{domain}\tIN\tDNSKEY\t{flag} 3 {algo[0]} {base64.b64encode(dnskey.key).decode()} ;{{id = {keytag} ({keyType}), size = {size}b}}\n")
    if args.ds:
        print(f"Writing DS RR to '{outDS}'")
        outDS.write_text(f"{domain}\tIN\tDS\t{ds.to_text()}\n")

