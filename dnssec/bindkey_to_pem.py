#!/usr/bin/env python3
"""Takes a private key generated using dnssec-keygen from Bind
(or ldns-keygen from ldns) in Bind's private key format and
writes it in a PEM-encoded file using PKCS8 format
The key file created is only readable by its owner and is not writable"""

import sys
import argparse
import base64

from getpass import getpass
from pathlib import Path

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa, ec, ed25519, ed448

parser = argparse.ArgumentParser(description="""Takes a Bind9-formatted key
                                and writes a PEM-encoded file in PKCS8 format. Output file has '0400' permission""")
parser.add_argument("-i", "--input", help="""Key file to transform. Suffix ".private" is optionnal,
                    the base name of the file K<name>+<alg>+<id> is enough""",
                    required=True, metavar="FILE", type=Path)
parser.add_argument("-o", "--output", help="File to write. Default: <input>.pem", metavar="FILE", type=Path)
parser.add_argument("-e", "--encrypt", help="Encrypt output file. User will be prompted for PEM pass phrase",
                    action="store_true")
parser.add_argument("-f", "--force", help="Force overwriting ouput file if it already exists", action="store_true")
args = parser.parse_args()

if args.input.suffix == ".private":
    keyFile = args.input.resolve()
else:
    keyFile = args.input.with_suffix(args.input.suffix + ".private").resolve()

if args.output:
    pemFile = args.output.resolve()
else:
    pemFile = keyFile.with_suffix(".pem").resolve()

class RFC8624Error(Exception):
    """Custom exception because why not?"""

def input_password() -> bytes:
    while True:
        p1 = getpass("Enter PEM pass phrase:").encode()
        p2 = getpass("Verifying - Enter PEM pass phrase:").encode()
        if p1 == p2:
            return p1
        else:
            print("Error: password did not match, please retry")
            continue

def b64_to_int(blob: str) -> int:
    """Takes a PEM-encoded base64 blob string and turns it into a int

    Necessary for converting numbers which are binary base64-encoded in BIND key format
    """
    return int.from_bytes(base64.b64decode(blob), byteorder="big")

def find_key_type(algo: int) -> str:
    """Takes a DNSSEC algorithm number and returns the algorith type to be used

    Sticks to RFC 8624 section 3.1 recommendations ("DNSSEC Signing" column)
    """
    if algo in (1, 3, 6, 12):
        raise RFC8624Error(f"""ERROR: Algorithm {algo} MUST NOT be used for DNSSEC Signing. See RFC 8624, section 3.1""")
    elif algo in (5, 7, 10):
        raise RFC8624Error(f"""ERROR: Algorithm {algo} is NOT RECOMMENDED to be used for DNSSEC Signing. See RFC 8624, section 3.1""")
    elif algo == 8:
        return "rsa"
    elif algo in (13, 14):
        return "ec"
    elif algo == 15:
        return "ed25519"
    elif algo == 16:
        return "ed448"
    else:
        raise ValueError(f"ERROR: {algo} is not valid DNSSEC algorithm")

if __name__ == "__main__":
    if not keyFile.exists():
        raise FileNotFoundError(f"No such file '{keyFile}'")
    if pemFile.exists():
        if args.force:
            pemFile.chmod(0o600)
        else:
            confirm = input(f"WARNING: '{pemFile}' already exists\nOverwrite it? (y/N)")
            if confirm in ("y", "Y"):
                pemFile.chmod(0o600)
            else:
                print("Not overwriting file. Bye")
                sys.exit(2)
    print(f"Loading {keyFile}...")
    lines = keyFile.read_text().splitlines()
    algo = int(lines[1].split()[1])
    try:
        kType = find_key_type(algo)
    except Exception as err:
        print(err)
        sys.exit(1)
    if kType == "rsa":
        print(f"{keyFile.parts[-1]} contains a RSA key...")
        # Public exponent should be equal to 65537 or 'AQAB' in the key encoding
        if lines[3].split()[1] != "AQAB":
            raise ValueError("Public exponent is not 65537!")
        e = 65537 # Public exponent
        n = b64_to_int(lines[2].split()[1]) # Modulus
        d = b64_to_int(lines[4].split()[1]) # Private Exponent
        p = b64_to_int(lines[5].split()[1]) # Prime1
        q = b64_to_int(lines[6].split()[1]) # Prime 2
        dmp1 = b64_to_int(lines[7].split()[1]) # Exponent 1
        dmq1 = b64_to_int(lines[8].split()[1]) # Exponent 2
        iqmp = b64_to_int(lines[9].split()[1]) # Coefficient
        privateKey = rsa.RSAPrivateNumbers(p, q, d, dmp1, dmq1, iqmp, rsa.RSAPublicNumbers(e, n)).private_key()
    elif kType == "ec":
        if algo == 13:
            print(f"{keyFile.parts[-1]} contains a ECDSA (P-256 curve) key...")
            curve = ec.SECP256R1()
        else:
            print(f"{keyFile.parts[-1]} contains a ECDSA (P-384 curve) key...")
            curve = ec.SECP384R1()
        order = b64_to_int(lines[2].split()[1])
        privateKey = ec.derive_private_key(order, curve)
    elif kType == "ed25519":
        print(f"{keyFile.parts[-1]} contains a ed25519 key...")
        privateKey = ed25519.Ed25519PrivateKey.from_private_bytes(base64.decodebytes(lines[2].split()[1].encode()))
    elif kType == "ed448":
        print(f"{keyFile.parts[-1]} contains a ed448 key...")
        privateKey = ed448.Ed448PrivateKey.from_private_bytes(base64.decodebytes(lines[2].split()[1].encode()))
    else:
        raise RuntimeError("Hmmm, how did we get here?")

    if args.encrypt:
        pem = privateKey.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.BestAvailableEncryption(input_password()))
    else:
        pem = privateKey.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption())

    print(f"Writing output to {pemFile}...")
    pemFile.write_bytes(pem)
    pemFile.chmod(0o400)

