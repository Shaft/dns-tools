# DNS Tools

Various DNS tools I made for fun and non-profit.

Please note that I write them mainly to learn Python (or may be some other language later) and DNS stuff. So:
- Expect badly written Python
- Scripts may break things (but less than a regular cat) - and may be broken themselves.

## check_soa.py and check_soa_multi.py

Those 2 scripts were moved to a [separate repository](https://framagit.org/Shaft/check-soa/)

## DNSSEC

- [bindkey_to_pem.py](dnssec/bindkey_to_pem.py): takes a private key file created with BIND's dnssec-keygen (or other similar tools) and turns it into a PEM-encoded file in PKCS8 format
- [pem_to_bindkey.py](dnssec/pem_to_bindkey.py): does the opposite. Takes a PEM encoded private key file and turns it into BIND's private key format. Also generates a DNSKEY RR (for either a ZSK or a KSK) and optionally a DS RR (all written in separate files).

## LICENSE

GNU GPLv3
